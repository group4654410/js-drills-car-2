function olderCarsYears(inventory, carYears, limit) {
    
    if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0 || carYears === undefined || Array.isArray(carYears) === false || carYears.length === 0 || limit === undefined || Number.isNaN(limit)) {
        return;
    }

    let oldCarsData = inventory.filter((car) => {
        if (car.car_year < limit) {
            return car;
        }
    });

    console.log(oldCarsData.length);
    return oldCarsData;
}

module.exports = olderCarsYears;
