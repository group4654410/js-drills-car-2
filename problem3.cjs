function sortedCarNames(inventory) {

  if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0) {
    return;
  }

  let car_models = inventory.map((car) => {
    return car.car_model;
  })
    .sort((first, second) => {
      return first.toLowerCase().localeCompare(second.toUpperCase());
    });

  console.log(car_models.join("\n"));
  return car_models;
}

module.exports = sortedCarNames;
