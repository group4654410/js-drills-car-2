function allCarYears(inventory) {

    if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0) {
        return;
    }

    let car_years = inventory.map((car) => {
        return car.car_year;
    });

    console.log(car_years.join("\n"));
    return car_years;
}

module.exports = allCarYears;
