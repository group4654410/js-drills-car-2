let assert = require("assert");

const inventory = require("../data.cjs");
const problem6 = require("../problem6.cjs");

let result = [{ id: 25, car_make: 'BMW', car_model: '525', car_year: 2005 },
{ id: 30, car_make: 'BMW', car_model: '6 Series', car_year: 2010 }];

assert.deepEqual(problem6(inventory, ["BMW"]), result);
