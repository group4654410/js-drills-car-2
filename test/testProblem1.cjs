let assert = require("assert");

const inventory = require("../data.cjs");
const problem1 = require("../problem1.cjs");

let result = { id: 10, car_make: 'Mercedes-Benz', car_model: 'E-Class', car_year: 2000 };
assert.deepEqual(problem1(inventory, 10), result);
