let assert = require("assert");

// const inventory = require("../data.cjs");
const problem3 = require("../problem3.cjs");

let data = [{ "id": 1, "car_make": "Lincoln", "car_model": "Navigator", "car_year": 2009 },
{ "id": 2, "car_make": "Mazda", "car_model": "Miata MX-5", "car_year": 2001 },
{ "id": 3, "car_make": "Land Rover", "car_model": "Defender Ice Edition", "car_year": 2010 },
{ "id": 4, "car_make": "Honda", "car_model": "Accord", "car_year": 1983 },
{ "id": 5, "car_make": "Mitsubishi", "car_model": "Galant", "car_year": 1990 }];

let result = ['Accord', 'Defender Ice Edition', 'Galant', 'Miata MX-5', 'Navigator'];
assert.deepEqual(problem3(data), result);
