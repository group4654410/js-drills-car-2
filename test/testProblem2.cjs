let assert = require("assert");

const inventory = require("../data.cjs");
const problem2 = require("../problem2.cjs");

let result = {"id":50,"car_make":"Lincoln","car_model":"Town Car","car_year":1999};
assert.deepEqual(problem2(inventory), result);
