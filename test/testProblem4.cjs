let assert = require("assert");

// const inventory = require("../data.cjs");
const problem4 = require("../problem4.cjs");

let data = [{ "id": 1, "car_make": "Lincoln", "car_model": "Navigator", "car_year": 2009 },
{ "id": 2, "car_make": "Mazda", "car_model": "Miata MX-5", "car_year": 2001 },
{ "id": 3, "car_make": "Land Rover", "car_model": "Defender Ice Edition", "car_year": 2010 },
{ "id": 4, "car_make": "Honda", "car_model": "Accord", "car_year": 1983 },
{ "id": 5, "car_make": "Mitsubishi", "car_model": "Galant", "car_year": 1990 }];

let result = [2009, 2001, 2010, 1983, 1990];
assert.deepEqual(problem4(data), result);
