function selctModels(inventory, carModels) {

    let selectCarData = [];

    if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0 || carModels === undefined || Array.isArray(carModels) === false || carModels.length === 0) {
        return;
    }

    selectCarData = inventory.filter((car) => {
        if (carModels.includes(car.car_make)) {
            console.log(JSON.stringify(car, null, "    "));
            return car;
        }
    });

    return selectCarData;
}

module.exports = selctModels;
