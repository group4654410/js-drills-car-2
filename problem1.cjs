function recallCarDetails(inventory, car_id) {

  if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0 || car_id === undefined || Number.isInteger(car_id) === false) {
    return [];
  }

  let car = inventory.filter((car) => {
    return car.id === car_id;
  })[0];

  console.log(`Car 33 is a ${car.car_year} ${car.car_make} ${car.car_model}`);
  return [car];

}

module.exports = recallCarDetails;
