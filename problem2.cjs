function lastCarDetails(inventory) {
  if (inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0) {
    return;
  }

  let lastCar = inventory.reduce((accumulator, currentValue) => {
    return currentValue;
  });

  console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
  return lastCar;
}

module.exports = lastCarDetails;
